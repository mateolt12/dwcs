<!DOCTYPE html >
<html>
    <head>
        <meta  charset="UTF-8">

        <title>Plantilla </title>
        <link href="dwcs.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <?php
        $db = getConexion();
        $arrayElementos = getElementos($db);
        ?>
        <div id="encabezado">
            <h1>Exercicio: mostraStockProduto</h1>
            <select name="selector" id="selector">
                <?php
                foreach ($arrayElementos as $posicion => $clave) {

                    echo "<option value='" . $clave['nome_corto'] . "'>" . $clave['nome_corto'] . "</option>";
                }
                ?>
            </select>
            <button id="mostrar" name="mostrar">Mostrar stock</button>
            <form id="form_seleccion" action="<?php echo $_SERVER['PHP_SELF']; ?>" method="post">
            </form>
        </div>
        <div id="contido">
            <h2>Stock do producto nas tendas:</h2>

        </div>
        <div id="pe">
        </div>
    </body>
</html>

<?php

function getConexion() {
    try {
        $db = new PDO('mysql:host=localhost;dbname=dwcs;charset=utf8', 'dwcs', 'abc123.');
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    } catch (PDOException $e) {
        echo "Error: RIP conexión " . $e->getMessage();
    }

    return $db;
}

function getElementos($db) {
    $aux = array();
    try {
        $resultado = $db->query("SELECT nome_corto FROM produto");
        while ($rexistro = $resultado->fetch()) {
            $aux[] = $rexistro;
        }
    } catch (PDOException $e) {
        echo "Error:" . $e->getMessage();
    }

    return $aux;
}
?>