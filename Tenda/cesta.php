<!DOCTYPE html>
<?php
if (isset($_POST['pagar'])) {
    header("Location: pagar.php");
}
if (isset($_POST['continuar'])) {
    header("Location: produtos.php");
}
?>

<html  lang="es">
    <head>
        <style>

            table, tr, td{
                border: 2px black solid;
                border-collapse: collapse;
            }

            tr td:last-child{
                text-align: center;
            }
        </style>
    </head>
    <body>

        <form action="logoff.php" method="POST">
            <?php
            include("config.php");
            session_start();
            $produtosFich = json_decode(file_get_contents(PRODUTOS_FILENAME), true);
            echo "Ola " . $_SESSION["user"] . "!";
            ?>
            <input type="submit" value="Pechar sesión" name="pecharSesion"/>
        </form>

        Cesta: <br>
        <form action='produtos.php' method='POST'>
            <input type='hidden' name='eliminar'>
            <table>
                <tr>
                    <td>Código</td>
                    <td>Nome</td>
                    <td>Uds</td>
                    <td>Prezo</td>
                    <td></td>
                </tr>
                <?php
                $totalEuros = 0;
                foreach ($_SESSION['cesta'] as $codigo => $valores) {
                    echo "<tr><td>" . $codigo . "</td>";
                    echo "<td>" . $valores["nome"] . "</td>";
                    echo "<td>" . $valores["uds"] . "</td>";
                    echo "<td>" . $valores["prezo"] . "€</td>";

                    $totalEuros += $valores["prezo"] * $valores["uds"];
                    echo "<td><input type='submit' name='$codigo' value='eliminar'></td></tr>";
                }

                echo "<tr><td></td><td></td><td>Total:</td><td>" . $totalEuros . "€</td><td></td></tr>";
                ?>
            </table>
        </form>   

        <form action='cesta.php' method='POST'>
            <input type='submit' name='continuar' value='Continuar enchendo a Cesta'>
            <input type='submit' name='pagar' value='Pagar'>
        </form>

    </body>
</html>

