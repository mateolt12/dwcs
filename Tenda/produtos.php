<!DOCTYPE html>

<html  lang="es">
    <head>
        <meta charset="UTF-8">
        <title>Tenda virtual - Produtos</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>

            table, tr, td{
                border: 2px black solid;
                border-collapse: collapse;
            }

            tr td:last-child{
                text-align: center;
            }


        </style>
    </head>
    <body>

        <form action="logoff.php" method="POST">
            <?php
            include("config.php");
            session_start();
            $produtosFich = json_decode(file_get_contents(PRODUTOS_FILENAME), true);
            echo "Ola " . $_SESSION["user"] . "!";
            ?>
            <input type="submit" value="Pechar sesión" name="pecharSesion"/>
        </form>

        <?php
        if (isset($_POST["engadir"])) {
            $codigoProductoPost = array_keys($_POST);
            $articuloEngadido = $codigoProductoPost[2];
            $pos = buscarIndice($articuloEngadido, $produtosFich);

            echo "<pre>";
            print_r($codigoProductoPost);
            print_r($_POST);
            echo "</pre>";

            if (!is_array($_SESSION['cesta'])) {
                $_SESSION['cesta'] = array();
            }

            if (array_key_exists($articuloEngadido, $_SESSION["cesta"])) {
                $_SESSION["cesta"][$articuloEngadido]["uds"] += $_POST["cantidad"][$articuloEngadido];
            } else {
                $_SESSION["cesta"][$articuloEngadido]["codigo"] = $produtosFich[$pos]["cod"];
                $_SESSION["cesta"][$articuloEngadido]["nome"] = $produtosFich[$pos]["nome_corto"];
                $_SESSION["cesta"][$articuloEngadido]["uds"] = $_POST['cantidad'][$articuloEngadido];
                $_SESSION["cesta"][$articuloEngadido]["prezo"] = $produtosFich[$pos]["PVP"];
                
                echo "<pre>";
                print_r($_SESSION);
                echo "</pre>";
            }
        }

        if (isset($_POST['eliminar'])) {
            $codigoProductoPost = array_keys($_POST);
            $articuloEngadido = $codigoProductoPost[1];
            unset($_SESSION['cesta'][$articuloEngadido]);
        }

        if (isset($_POST['vaciar'])) {
            $_SESSION['cesta'] = array();
        }
        
        if(isset($_POST["comprar"])){
            header("Location: cesta.php");
        }

        if (isset($_SESSION['cesta']) && count($_SESSION['cesta']) != 0) {
            $totalEuros = 0;
            ?>
        
            Cesta: <br>
            <form action='produtos.php' method='POST'>
                <input type='hidden' name='eliminar'>
                <table>
                    <tr>
                        <td>Código</td>
                        <td>Nome</td>
                        <td>Uds</td>
                        <td>Prezo</td>
                        <td></td>
                    </tr>
                    <?php
                    foreach ($_SESSION['cesta'] as $codigo => $valores) {
                        echo "<tr><td>" . $codigo . "</td>";
                        echo "<td>" . $valores["nome"] . "</td>";
                        echo "<td>" . $valores["uds"] . "</td>";
                        echo "<td>" . $valores["prezo"] . "€</td>";
                        
                        $totalEuros += $valores["prezo"] * $valores["uds"];
                        echo "<td><input type='submit' name='$codigo' value='eliminar'></td></tr>";
                    }

                    echo "<tr><td></td><td></td><td>Total:</td><td>" . $totalEuros . "€</td><td></td></tr>";
                    ?>
                </table>
            </form>   
   
            <form action='produtos.php' method='POST'>
                <input type='submit' name='vaciar' value='Vaciar cesta'>
                <input type='submit' name='comprar' value='Comprar'>
            </form>
            
            
            <?php
        } else {
            $_SESSION['cesta'] = array();
        }
        ?>

        <br><br>
        
        <form action ="produtos.php" method="post">
            Productos para mercar: <br><br><br>
            <table>
                <tr>
                    <td>Codigo</td>
                    <td>Nome</td>
                    <td>PVP</td>
                    <td>Cantidade</td>
                    <td>Engadir</td>
                </tr>
                <input type="hidden" name="engadir">
                <?php
                for ($i = 0; $i <= count($produtosFich) - 1; $i++) {
                    $codigo = $produtosFich[$i]["cod"];
                    echo "<tr><td>" . $produtosFich[$i]["cod"]
                    . "</td><td>" . $produtosFich[$i]["nome_corto"]
                    . "</td><td>" . $produtosFich[$i]["PVP"]
                    . "</td><td><input type='number' min='1' name='cantidad[$codigo]' value='1'>"
                    . "</td><td><input type='submit' name='$codigo' value='engadir' ></td></tr>";
                }
                ?>
            </table>
        </form>
    </body>
</html>

<?php

function buscarIndice($producto, $produtosFich) {
    foreach ($produtosFich as $clave => $valores) {
        if ($valores["cod"] == $producto)
            return $clave;
    }
}
?>